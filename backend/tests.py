#!/usr/bin/env python3

import unittest
import database
from database import *
from flask import Flask
from flask_testing import TestCase
import pprint 
import requests
from mock import patch, MagicMock
from app import *
import flask

pp = pprint.PrettyPrinter(indent=4)

class UnitTests(TestCase):
    SQLALCHEMY_DATABASE_URI = "sqlite://"
    TESTING = True
    render_templates = False

    def create_app(self):
         # Default port is 5000
        app.config['TESTING'] = True
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
        db.init_app(app)
        return app

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    """
    test for app.py
    """
    
    def test_create(self):
        """
        test for create() - which creates tables in the database
        could check that all 5 tables are created!
        """
        tbl_names = ["pet", "vet", "shelter", "bred", "close_vet_shelter"]
        response = self.client.get('/create_table')
        self.assertEqual(response.status_code, 200)

        # TODO: Add tests to verify that all the tables are created

    
    def test_insert(self):
        """
        test for insert()
        """
        # this will insert data into an in-memory database
        # check if data is inserted into primary tables: pet, vet, and shelter.

        response = self.client.get('/insert_data')
        self.assertEqual(response.status_code, 200)

        # check that some pets, vets and shelters were inserted

    def test_msg_no_content(self):
        """
        test for msg_no_content()
        """
        # NEED TO WORK ON 
        pass

    def test_parse_params(self):
        """
        test for parse_params()
        """
        input = {
            'animal': 'dog',
            'state': 'TX'
            }
        expect1 = {'animal': 'dog'}
        expect2 = {'state': 'TX'}
        pet_cond, shel_cond = parse_params(input)
        self.assertEqual(expect1, pet_cond)
        self.assertEqual(expect2, shel_cond)

    """
    test for API endpoints
    """

    """
    Endpoint for Pet
    """
    def test_api_PetPage_no_param_provided(self):
        """
        test for PetPage()

        Default state=TX
        Should return Pet(state=TX) since no params are provided

        endpoint: /api/pet/page/1
        """
        expected =\
            {   'objects': [   {   'age': None,
                       'all_images': None,
                       'animal': 'dog',
                       'breed': None,
                       'description': None,
                       'id': '1',
                       'image_url': None,
                       'name': None,
                       'sex': None,
                       'shelter': '1',
                       'size': None,
                       'vet': None},
                   {   'age': None,
                       'all_images': None,
                       'animal': 'dog',
                       'breed': None,
                       'description': None,
                       'id': '2',
                       'image_url': None,
                       'name': None,
                       'sex': None,
                       'shelter': '1',
                       'size': None,
                       'vet': None},
                   {   'age': None,
                       'all_images': None,
                       'animal': 'cat',
                       'breed': None,
                       'description': None,
                       'id': '3',
                       'image_url': None,
                       'name': None,
                       'sex': None,
                       'shelter': '1',
                       'size': None,
                       'vet': None}],
                'page': 1,
                'total_pages': 1}
        db.session.add(Pet(id=1, shelter_id=1, animal='dog'))
        db.session.add(Pet(id=2, shelter_id=1, animal='dog'))
        db.session.add(Pet(id=3, shelter_id=1, animal='cat'))
        db.session.add(Pet(id=4, shelter_id=2, animal='dog'))
        db.session.add(Pet(id=5, shelter_id=2, animal='dog'))
        db.session.add(Shelter(id=1, state='TX', city='Austin'))
        db.session.add(Shelter(id=2, state='SC', city='Aiken'))
        db.session.add(Shelter(id=3, state='TX', city='Dallas'))
        db.session.commit()
        response = self.client.get('/api/pet/page/1')
        # pp.pprint(response.json)
        self.assertEqual(response.json, expected)
        self.assertEqual(response.status_code, 200)

    def test_api_PetPage_param_city_animal_provided(self):
        """
        test for PetPage()

        Should return only Pet instances (animal=dog & city=Austin)
        Also, default state=TX

        endpoint: /api/pet/page/1?animal=dog&city=Austin
        """
        expected =\
            {'objects': [   {   'age': None,
                       'all_images': None,
                       'animal': 'dog',
                       'breed': None,
                       'description': None,
                       'id': '1',
                       'image_url': None,
                       'name': None,
                       'sex': None,
                       'shelter': '1',
                       'size': None,
                       'vet': None},
                   {   'age': None,
                       'all_images': None,
                       'animal': 'dog',
                       'breed': None,
                       'description': None,
                       'id': '2',
                       'image_url': None,
                       'name': None,
                       'sex': None,
                       'shelter': '1',
                       'size': None,
                       'vet': None}],
                'page': 1,
                'total_pages': 1}
        db.session.add(Pet(id=1, shelter_id=1, animal='dog'))
        db.session.add(Pet(id=2, shelter_id=1, animal='dog'))
        db.session.add(Pet(id=3, shelter_id=1, animal='cat'))
        db.session.add(Pet(id=4, shelter_id=2, animal='dog'))
        db.session.add(Pet(id=5, shelter_id=2, animal='dog'))
        db.session.add(Shelter(id=1, state='TX', city='Austin'))
        db.session.add(Shelter(id=2, state='TX', city='Dallas'))
        db.session.add(Shelter(id=3, state='TX', city='Dallas'))
        db.session.commit()
        response = self.client.get('/api/pet/page/1?animal=dog&city=Austin')
        # pp.pprint(response.json)
        self.assertEqual(response.json, expected)
        self.assertEqual(response.status_code, 200)

    """
    Endpoint for Pet
    """
    def test_api_VetID(self):
        """
        test for VetID()

        make sure this endpoint return Vet Class instance
        in json based on vet's id

        endpoint: /api/vet/<string:vid>
        """
        # NEED WORK ON 
        pass

    """
    Endpoint for Pet
    """
    def test_api_ShelterID(self):
        """
        test for ShelterID()

        make sure this endpoint return Shelter Class instance
        in json based on shelter's id 

        endpoint: /api/shelter/<string:sid>
        """
        # NEED WORK ON 
        pass
            
    """
    Endpoint for Dropdown
    """
    def test_api_DropdownPet_no_param_provided(self):
        """
        test for DropdownPet()

        Default state = TX, so if no param provided, then
        return list of city for TX

        endpoint: /api/pet/dropdown
        """
        expected = {'city': ['Austin', 'Dallas']}
        db.session.add(Shelter(id=1, state='TX', city='Austin'))
        db.session.add(Shelter(id=2, state='TX', city='Dallas'))
        db.session.add(Shelter(id=3, state='TX', city='Dallas'))
        db.session.commit()
        response = self.client.get('/api/pet/dropdown')
        # pp.pprint(response.json)
        self.assertEqual(response.json, expected)
        self.assertEqual(response.status_code, 200)

    def test_api_DropdownPet_param_state_provided(self):
        """
        test for DropdownPet()

        return: list of city for SC
        endpoint: /api/pet/dropdown?state=SC
        """
        expected = {'city': ['Aiken', 'Rock Hill']}
        db.session.add(Shelter(id=1, state='TX', city='Austin'))
        db.session.add(Shelter(id=2, state='SC', city='Aiken'))
        db.session.add(Shelter(id=3, state='SC', city='Rock Hill'))
        db.session.commit()
        response = self.client.get('/api/pet/dropdown?state=SC')
        # pp.pprint(response.json)
        self.assertEqual(response.json, expected)
        self.assertEqual(response.status_code, 200)


    """
    test for database.py
    """
    def test_count_total_zip(self):
        """
        test count_total_zip function
            - count number of zips from middlezip.txt
            - return total number of zipcodes
        """
        filename = 'middlezip.txt'
        total_zip = count_total_zip(filename)
        self.assertEqual(total_zip, 3)

    def test_insert_vet(self):
        """
        test insert_vet function
            - make sure vet are inserted into session
        """
        with patch.object(getPetsUtil, 'getVetsByLocation') as mock_getVetsByLocation:
            expected_return = Vet(id="1")
            zipcode = 78731
            # mock calling getVetsByLocation!  returns a list of Vets
            mock_getVetsByLocation.return_value = [expected_return]
            insert_vet(zipcode)
            db.session.commit()
            vets = Vet.query.all()
            for v in vets:
                self.assertEqual(expected_return, v)

    def test_insert_shelter(self):
        """
        test insert_shelter function
            - make sure vet are inserted into session
            - make sure the return function returns a set of pets
              to delete (pets that have no shelter.
        """
        with patch.object(getPetsUtil, 'getShelterByIdFromPetFinder') as mock_getShelterByIdFromPetFinder:
            expected_return = Shelter(id="1")
            input_pet = [Pet(id=1)]
            zipcode = 78731
            mock_getShelterByIdFromPetFinder.return_value = expected_return
            deleted_pet = insert_shelter(input_pet)
            db.session.commit()
            shelters = Shelter.query.all()
            for s in shelters:
                self.assertEqual(expected_return, s)
            self.assertEqual(deleted_pet, set())

    # NEED TO WORK ON BUGYYY
    def test_insert_close_vet_shelter_bridgeTable(self):
        """
        test insert_close_vet_shelter_bridgeTable
            - a pair close vet and shelter should be inserted
        """
        with patch.object(database, 'db') as mock_v:
            with patch.object(database, 'db') as mock_s:
                mock_v.session.query(Vet).all.return_value = [Vet(id='1')]
                mock_s.session.query(Shelter).filter_by.return_value = [Shelter(id='1')]
            
    def test_calculate_distance(self):
        """
        test for calculate_distance
            - calculate distance btw two points
        """
        mile = calculate_distance(lat_1=30, log_1=30, lat_2=40, log_2=40)
        self.assertEqual(int(mile), 891)

    """
    test for database.py
    """
    def test_count_total_zip(self):
        """
        test count_total_zip function
            - count number of zips from middlezip.txt
            - return total number of zipcodes
        """
        filename = 'middlezip.txt'
        total_zip = count_total_zip(filename)
        self.assertEqual(total_zip, 3)

    """
    Tests for App.py
    """

    # @patch(app.render)
    def test_render(self):
        """
        test for render()
        What would we test?  I don't think we need this.
        """
        # app.render.return_value = 200

        # NEED TO WORK ON 
        pass

    def test_num_pages(self):
        """
        test for num_pages()
        test cases:
        pageSize  numIn  numPages
        9          100      12
        9          9        1
        9          10       2
        """
        self.assertEqual(12, num_pages(100))
        self.assertEqual(1, num_pages(9))
        self.assertEqual(2, num_pages(10))

    def test_filter_by(self):
        """
        test for filter_by()
        """
        # NEED TO WORK ON 
        pass

    def test_msg_no_content(self):
        """
        test for msg_no_content()
        """
        # NEED TO WORK ON 
        pass

    def test_parse_params(self):
        """
        test for parse_params()
        """
        input = {
            'animal': 'dog',
            'state': 'TX'
            }
        expect1 = {'animal': 'dog'}
        expect2 = {'state': 'TX'}
        pet_cond, shel_cond = parse_params(input)
        self.assertEqual(expect1, pet_cond)
        self.assertEqual(expect2, shel_cond)

    """
    test for API endpoints
    """

    """
    Endpoint for Pet
    """
    def test_api_PetPage_no_param_provided(self):
        """
        test for PetPage()

        Default state=TX
        Should return Pet(state=TX) since no params are provided

        endpoint: /api/pet/page/1
        """
        expected =\
            {   'objects': [   {   'age': None,
                       'all_images': None,
                       'animal': 'dog',
                       'breed': None,
                       'description': None,
                       'id': '1',
                       'image_url': None,
                       'name': None,
                       'sex': None,
                       'shelter': '1',
                       'size': None,
                       'vet': None},
                   {   'age': None,
                       'all_images': None,
                       'animal': 'dog',
                       'breed': None,
                       'description': None,
                       'id': '2',
                       'image_url': None,
                       'name': None,
                       'sex': None,
                       'shelter': '1',
                       'size': None,
                       'vet': None},
                   {   'age': None,
                       'all_images': None,
                       'animal': 'cat',
                       'breed': None,
                       'description': None,
                       'id': '3',
                       'image_url': None,
                       'name': None,
                       'sex': None,
                       'shelter': '1',
                       'size': None,
                       'vet': None}],
                'page': 1,
                'total_pages': 1}
        db.session.add(Pet(id=1, shelter_id=1, animal='dog'))
        db.session.add(Pet(id=2, shelter_id=1, animal='dog'))
        db.session.add(Pet(id=3, shelter_id=1, animal='cat'))
        db.session.add(Pet(id=4, shelter_id=2, animal='dog'))
        db.session.add(Pet(id=5, shelter_id=2, animal='dog'))
        db.session.add(Shelter(id=1, state='TX', city='Austin'))
        db.session.add(Shelter(id=2, state='SC', city='Aiken'))
        db.session.add(Shelter(id=3, state='TX', city='Dallas'))
        db.session.commit()
        response = self.client.get('/api/pet/page/1')
        # pp.pprint(response.json)
        self.assertEqual(response.json, expected)
        self.assertEqual(response.status_code, 200)

    def test_api_PetPage_param_city_animal_provided(self):
        """
        test for PetPage()

        Should return only Pet instances (animal=dog & city=Austin)
        Also, default state=TX

        endpoint: /api/pet/page/1?animal=dog&city=Austin
        """
        expected =\
            {'objects': [   {   'age': None,
                       'all_images': None,
                       'animal': 'dog',
                       'breed': None,
                       'description': None,
                       'id': '1',
                       'image_url': None,
                       'name': None,
                       'sex': None,
                       'shelter': '1',
                       'size': None,
                       'vet': None},
                   {   'age': None,
                       'all_images': None,
                       'animal': 'dog',
                       'breed': None,
                       'description': None,
                       'id': '2',
                       'image_url': None,
                       'name': None,
                       'sex': None,
                       'shelter': '1',
                       'size': None,
                       'vet': None}],
                'page': 1,
                'total_pages': 1}
        db.session.add(Pet(id=1, shelter_id=1, animal='dog'))
        db.session.add(Pet(id=2, shelter_id=1, animal='dog'))
        db.session.add(Pet(id=3, shelter_id=1, animal='cat'))
        db.session.add(Pet(id=4, shelter_id=2, animal='dog'))
        db.session.add(Pet(id=5, shelter_id=2, animal='dog'))
        db.session.add(Shelter(id=1, state='TX', city='Austin'))
        db.session.add(Shelter(id=2, state='TX', city='Dallas'))
        db.session.add(Shelter(id=3, state='TX', city='Dallas'))
        db.session.commit()
        response = self.client.get('/api/pet/page/1?animal=dog&city=Austin')
        # pp.pprint(response.json)
        self.assertEqual(response.json, expected)
        self.assertEqual(response.status_code, 200)

    """
    Endpoint for Pet
    """
    def test_api_VetID(self):
        """
        test for VetID()

        make sure this endpoint return Vet Class instance
        in json based on vet's id

        endpoint: /api/vet/<string:vid>
        """
        # NEED WORK ON 
        pass

    """
    Endpoint for Pet
    """
    def test_api_ShelterID(self):
        """
        test for ShelterID()

        make sure this endpoint return Shelter Class instance
        in json based on shelter's id 

        endpoint: /api/shelter/<string:sid>
        """
        # NEED WORK ON 
        pass
            
    """
    Endpoint for Dropdown
    """
    def test_api_DropdownPet_no_param_provided(self):
        """
        test for DropdownPet()

        Default state = TX, so if no param provided, then
        return list of city for TX

        endpoint: /api/pet/dropdown
        """
        expected = {'city': ['Austin', 'Dallas']}
        db.session.add(Shelter(id=1, state='TX', city='Austin'))
        db.session.add(Shelter(id=2, state='TX', city='Dallas'))
        db.session.add(Shelter(id=3, state='TX', city='Dallas'))
        db.session.commit()
        response = self.client.get('/api/pet/dropdown')
        # pp.pprint(response.json)
        self.assertEqual(response.json, expected)
        self.assertEqual(response.status_code, 200)

    def test_api_DropdownPet_param_state_provided(self):
        """
        test for DropdownPet()

        return: list of city for SC
        endpoint: /api/pet/dropdown?state=SC
        """
        expected = {'city': ['Aiken', 'Rock Hill']}
        db.session.add(Shelter(id=1, state='TX', city='Austin'))
        db.session.add(Shelter(id=2, state='SC', city='Aiken'))
        db.session.add(Shelter(id=3, state='SC', city='Rock Hill'))
        db.session.commit()
        response = self.client.get('/api/pet/dropdown?state=SC')
        # pp.pprint(response.json)
        self.assertEqual(response.json, expected)
        self.assertEqual(response.status_code, 200)


    """
    test for getPetsUtil.py
    """

    def test_num(self):
        """
        test for num()
        """
        # NEED TO WORK ON 
        pass

    def test_unicode(self):
        """
        test for unicode()
        """
        # NEED TO WORK ON 
        pass

    def test_stringify(self):
        """
        test for stringify()
        """
        # NEED TO WORK ON 
        pass

    def test_getVetsByLocation(self):
        """
        test for getVetsByLocation()
        """
        # NEED TO WORK ON 
        # USE with patch.object()
        pass

    def test_getRandomPetFromPetFinder(self):
        """
        test for getRandomPetFromPetFinder()
        """
        # NEED TO WORK ON 
        # USE with patch.object()
        pass

    def test_parsePet(self):
        """
        test for parsePet()
        """
        # NEED TO WORK ON 
        pass

    def test_getPetsFromPetFinder(self):
        """
        test for getPetsFromPetFinder()
        """
        # NEED TO WORK ON 
        # USE with patch.object()
        pass

    def test_getPetsNonSpecific(self):
        """
        test for getPetsNonSpecific()
        """
        # NEED TO WORK ON 
        # USE with patch.object()
        pass

    def test_getShelterByIdFromPetFinder(self):
        """
        test for getShelterByIdFromPetFinder()
        """
        # NEED TO WORK ON 
        # USE with patch.object()
        pass

    def test_getPetByIdFromPetFinder(self):
        """
        test for getPetByIdFromPetFinder()
        """
        # NEED TO WORK ON 
        # USE with patch.object()
        pass


if __name__ == "__main__":
    unittest.main()
    # unittest.main(testRunner=HTMLTestRunner(output='example_dir'))
