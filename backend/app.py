#!/usr/bin/env python

import json
import os
import random
import mysql.connector
from flask import Flask, jsonify, render_template, request
from flask_restless import APIManager
from flask_sqlalchemy import SQLAlchemy
from mysql.connector import errorcode
from sqlalchemy import *

import database
from database import *
from flask_cors import CORS
from flask_marshmallow import Marshmallow
from flask_restful import Api, Resource, reqparse

app = Flask(__name__)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

# Set database info based on dev or production
prod = os.environ.get("PRODUCTION", None)
if not prod:
    db_name = os.environ.get("DEV_DB", None)
    username = os.environ.get("DEV_USERNAME", None)
    password = os.environ.get("DEV_PASSWORD", None)
    hostname = os.environ.get("DEV_HOSTNAME", None)
else:
    db_name = os.environ.get("RDS_DB_NAME", None)
    username = os.environ.get("RDS_USERNAME", None)
    password = os.environ.get("RDS_PASSWORD", None)
    hostname = os.environ.get("RDS_HOSTNAME", None)
options = "charset=utf8mb4"


# if you comment this in, you will connect production databse
db_name = os.environ.get("RDS_DB_NAME", None)
username = os.environ.get("RDS_USERNAME", None)
password = os.environ.get("RDS_PASSWORD", None)
hostname = os.environ.get("RDS_HOSTNAME", None)


# SQLAlchemy configurations
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# if db_name == None, don't connect to databse. This is useful for testing
if db_name != None:
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://' + username + \
        ':' + password + '@' + hostname + '/' + db_name + '?' + options
else:
    print("** Not connet to database for testing **")



# Initialize database and marshmallow
database.db.init_app(app)
database.ma.init_app(app)
# Initialize API
api = Api(app)


@app.route('/create_table', methods=['GET'])
def create():

    # calling function from database.py
    database.create_table(app)
    return render_template('index.html', message="created tables!!"), 200


@app.route('/insert_data', methods=['GET'])
def insert():

    # calling function from database.py
    database.insert_data(app)
    return render_template('index.html', message="Data inserted!!")


@app.route("/")
def render(message=None):
    return render_template('index.html', message=message), 200


# """""""""""
# API: Helper functions
# """""""""""

# Pagination
PER_PAGE = 9


def num_pages(n):
    return (n // PER_PAGE) if (n % PER_PAGE) == 0 else (n // PER_PAGE + 1)


def filter_by(item, id, many):
    if item == 'pid':
        item_str = 'pet'
        query = Pet.query.filter_by(id=id).first_or_404()
        query_schema = PetSchema(many=many)
    elif item == 'vid':
        item_str = 'vet'
        query = Vet.query.filter_by(id=id).first_or_404()
        query_schema = VetSchema(many=many)
    elif item == 'sid':
        item_str = 'shelter'
        query = Shelter.query.filter_by(id=id).first_or_404()
        query_schema = ShelterSchema(many=many)
    elif item == 'bid':
        item_str = 'breed'
        query = Breed.query.filter_by(id=id).first_or_404()
        query_schema = BreedSchema(many=many)
    else:
        return msg_no_content()
    return jsonify({item_str: query_schema.dump(query).data})


def msg_no_content():
    message = "{ERROR MSG : NO CONTENT FOUND, 204}"
    return jsonify({'ERROR MSG': 'NO CONTENT FOUND'}), 204


# """"""""""""
# API: Pet
# """"""""""""


class PetID(Resource):
    def get(self, pid=None):
        if not pid:
            return msg_no_content()
        return filter_by('pid', pid, False)


class PetAll(Resource):
    def get(self):
        query = Pet.query.all()
        if not query:
            return msg_no_content()
        query_schema = PetSchema(many=true)
        output = query_schema.dump(query)
        return jsonify({'pet': output.data}) 

def parse_params(params):
    """
    input: dict -> query parameter
    
    parse dict of query parametes and create three dicts
    1: condition for pet
    2: condition for shelter
    return 2 dicts 
    """
    p_condition = set(['animal'])
    s_condition = set(['city', 'state'])
    p_dict = {}
    # Default State = TX
    s_dict = {'state': 'TX'}
    for key, value in params.items():
        if key in p_condition and value:
            p_dict[key] = value
        elif key in s_condition and value:
            s_dict[key] = value

    return p_dict, s_dict

def add_query_params(parser, param_list):
    for param in param_list:
        parser.add_argument(param, type=str)

class PetPage(Resource):
    def get(self, pg=None):
        if not pg:
            return msg_no_content()
        
        # get Query parameters
        parser = reqparse.RequestParser()
        add_query_params(parser, ['animal', 'city', 'state'])
        pet_cond, shel_cond = parse_params(parser.parse_args())

        pages = database.db.session.query(Pet).filter_by(**pet_cond).\
                    join(Shelter, Pet.shelter_id == Shelter.id).filter_by(**shel_cond).\
                    paginate(page=pg, per_page=PER_PAGE, error_out=True)
        pages_schema = PetSchema(many=True)
        pets = pages_schema.dump(pages.items)
        return jsonify(
            {'objects': pets.data,
             'total_pages': num_pages(pages.total),
             'page': pages.page}
        )

# """"""""""""
# API: Vet
# """"""""""""


class VetID(Resource):
    def get(self, vid=None):
        if not vid:
            return msg_no_content()
        return filter_by('vid', vid, False)


class VetAll(Resource):
    def get(self):
        query = Vet.query.all()
        if not query:
            return msg_no_content()
        query_schema = VetSchema(many=True)
        output = query_schema.dump(query)
        return jsonify({'vet': output.data})


class VetPage(Resource):
    def get(self, pg=None):
        if not pg:
            return msg_no_content()
        pages = Vet.query.paginate(page=pg, per_page=PER_PAGE, error_out=true)
        pages_schema = VetSchema(many=True)
        vets = pages_schema.dump(pages.items)
        return jsonify(
            {'objects': vets.data,
             'total': num_pages(pages.total),
             'current_page': pages.page}
        )


class VetPageTotal(Resource):
    def get(self):
        pages = Vet.query.paginate(per_page=PER_PAGE, error_out=true)
        return jsonify({'total': num_pages(pages.total)})


class VetPet(Resource):
    def get(self, pid=None):
        vid = Pet.query.filter_by(id=pid).first_or_404().vet_id
        query = Vet.query.filter_by(id=vid).first_or_404()
        query_schema = VetSchema(many=False)
        output = query_schema.dump(query)
        return jsonify({'vet': output.data})


class VetShelter(Resource):
    def get(self, sid=None):
        table = database.db.session.query(close_vet_shelter)
        vidSQL = table.filter_by(shelter_id=sid).first_or_404()
        if not vidSQL:
            return msg_no_content()

        vid = vidSQL.vet_id
        query = Vet.query.filter_by(id=vid)
        query_schema = VetSchema(many=True, strict=True)
        output = query_schema.dump(query)
        return jsonify({'vet': output.data})


class VetTotal(Resource):
    def get(self):
        return jsonify({'vet_total': Vet.query.count()})


# """"""""""""
# API: Shelter
# """"""""""""


class ShelterID(Resource):
    def get(self, sid=None):
        if not sid:
            return msg_no_content()
        return filter_by('sid', sid, False)


class ShelterAll(Resource):
    def get(self):
        query = Shelter.query.all()
        if not query:
            return msg_no_content()
        query_schema = ShelterSchema(many=True)
        output = query_schema.dump(query)
        return jsonify({'shelter': output.data})


class ShelterPage(Resource):
    def get(self, pg=None):
        if not pg:
            return msg_no_content()
        pages = Shelter.query.paginate(
            page=pg, per_page=PER_PAGE, error_out=true)
        pages_schema = ShelterSchema(many=True)
        shelters = pages_schema.dump(pages.items)
        return jsonify(
            {'objects': shelters.data,
             'total': num_pages(pages.total),
             'current_page': pages.page}
        )


class ShelterPageTotal(Resource):
    def get(self):
        pages = Shelter.query.paginate(per_page=PER_PAGE, error_out=true)
        return jsonify({'total': num_pages(pages.total)})


class ShelterPet(Resource):
    def get(self, pid=None):
        sid = Pet.query.filter_by(id=pid).first_or_404().shelter_id
        query = Vet.query.filter_by(id=sid).first_or_404()
        query_schema = VetSchema(many=False)
        output = query_schema.dump(query)
        return jsonify({'shelter': output.data})


class ShelterVet(Resource):
    def get(self, vid=None):
        table = database.db.session.query(close_vet_shelter)
        sidSQL = table.filter_by(vet_id=vid).first_or_404()
        if not sidSQL:
            return msg_no_content()

        sid = sidSQL.shelter_id
        query = Vet.query.filter_by(id=sid)
        query_schema = VetSchema(many=True, strict=True)
        output = query_schema.dump(query)
        return jsonify({'shelter': output.data})


class ShelterTotal(Resource):
    def get(self):
        return jsonify({'shelter_total': Shelter.query.count()})


# """"""""""""
# API: Breed
# """"""""""""

class BreedID(Resource):
    """
    Return breed temperament and detail
    based on breed name
    """
    def get(self, bid=None):
        if not bid:
            return msg_no_content()
        return filter_by('bid', bid, False)

# """"""""""""
# API: Dropdown
# """"""""""""

class DropdownPet(Resource):
    """
    ex: state=TX -> return list of city for TX
        anima=Dog -> return list of breed for Dog
    """
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('state', type=str)

        _, shel_cond = parse_params(parser.parse_args())
        query = Shelter.query.with_entities(Shelter.city).\
                                filter_by(**shel_cond).distinct()
        states = [state[0] for state in query]

        return jsonify({'city': states})


# Pet API
api.add_resource(PetID, '/api/pet/<int:pid>')
api.add_resource(PetAll, '/api/pet/all')
api.add_resource(PetPage, '/api/pet/page/<int:pg>')

# Vet API
api.add_resource(VetID, '/api/vet/<string:vid>')
api.add_resource(VetAll, '/api/vet/all')
api.add_resource(VetPage, '/api/vet/page/<int:pg>')
api.add_resource(VetPageTotal, '/api/vet/page/total')
api.add_resource(VetPet, '/api/vet/pet/<int:pid>')
api.add_resource(VetShelter, '/api/vet/shelter/<int:sid>')
api.add_resource(VetTotal, '/api/vet/total')

# Shelter API
api.add_resource(ShelterID, '/api/shelter/<string:sid>')
api.add_resource(ShelterAll, '/api/shelter/all')
api.add_resource(ShelterPage, '/api/shelter/page/<int:pg>')
api.add_resource(ShelterPageTotal, '/api/shelter/page/total')
api.add_resource(ShelterPet, '/api/shelter/pet/<int:pid>')
api.add_resource(ShelterVet, '/api/shelter/vet/<int:vid>')
api.add_resource(ShelterTotal, '/api/shelter/total')

# Breed API
api.add_resource(BreedID, '/api/breed/<string:bid>')

# Dropdown API
api.add_resource(DropdownPet, '/api/pet/dropdown')

if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0')
