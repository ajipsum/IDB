# Overview
[ConnectPetsTo.Me](http://www.connectpetsto.me) is a website that allows visitors to find pets that they might want to adopt, or a shelter where they might like to volunteer.

# [Project's Class Page](https://www.cs.utexas.edu/users/downing/cs373/projects/IDB.html)

# [API Documentation](https://documenter.getpostman.com/view/4670195/RWMCt9Mi)

# [Technical Specification](https://docs.google.com/document/d/1i6461flSKyNWnF6pArVEBhRB87cRE7VhJwnB0YfxQkg/edit?usp=sharing)

# [UML (download to view)](https://gitlab.com/CS373-18SU-GRP/IDB/tree/master/uml)


# --------------------------------------------------------------------------------

# README: Class requirement section
##   Name, EID, and GitLab ID, of all members
    * Abdullah Abualsoud (AJ),    aa73228,    ajipsum
    * Alex Flores Escacega,       af28459,    afloresescarcega
    * David Mao,                  dm46452,    the_color_blurple
    * Jeffrey Zhu,                jwz269,     jeffwzhu
    * William Wesley Monroe,      wwm394,     purpleHey
    * Yoshinobu Nakada,           yn2536,     yoshi1579

## Git SHA
    a643fa21533f7e43276d36bed70deb45a9b90c5f

## Link to website:
[ConnectPetsTo.Me](http://www.connectpetsto.me)

## Estimated completion time (hours: int)
    * Abdullah Abualsoud (AJ) : 40
    * Alex Flores Escacega    : 20
    * David Mao               : 20
    * Jeffrey Zhu             : 25
    * Wesley Monroe           : 10
    * Yoshinobu Nakada        : 40

## Actual completion time (hours: int)
    * Abdullah Abualsoud (AJ) : 84
    * Alex Flores Escacega    : 35
    * David Mao               : 30
    * Jeffrey Zhu             : 30
    * William Wesley Monroe   : 40
    * Yoshinobu Nakada        : 55

## Comments
In this phase we implemented git workflow.  All our work is on branches.
This is our link to our user stories: [Link](https://gitlab.com/CS373-18SU-GRP/IDB/boards?=&label_name%5B%5D=Stories%20-%20Customer)
