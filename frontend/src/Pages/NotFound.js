import React, { Component } from 'react';

export default class NotFound extends Component {
  render() {
    return (
      <div className = "container">
        <h1> 404 PAGE NOT FOUND </h1>
      </div>
    )
  }
}