import React, { Component } from 'react';
import Vets from './../../../Components/Model/Vets/Vets';
import PageHeader from './../../../Components/PageHeader'
import PageBar from './../../../Components/PageBar';
import axios from 'axios';

export default class vetList extends Component {
    constructor(props) {
        super(props);
        this.state = {
          "totalPages": 1,
        };
    }
      
      // get total number of pages
    componentWillMount() {
        const url = 'http://localhost:5000/api/vet/page/total'
        axios.get(url, {
            headers: { 
                'Access-Control-Allow-Origin' : '*',
                'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',          
            },
            responseType: 'json',
        })
        .then(res => {
            this.setState({totalPages: res.data.total});
        });
    }

    render() {
        let pageNum = this.props.match.params.pageNum;
        
        return (
            <div className="album py-5 bg-light">
                <div className="container fillPage">
                    <PageHeader heading="Vets" domain="/FindVets/"
                     query={this.props.location.search}/>
                    <br />
                    <br />
                    <Vets currentPage={pageNum}/>
                </div>
                <PageBar domain="/FindVets/" pageNum={pageNum} totalPages = {this.state.totalPages}/>
            </div>
        )
    }
}
