import React, { Component } from 'react';
import Pets from './../../../Components/Model/Pets/Pets';
import PageBar from './../../../Components/PageBar';
import PageHeader from './../../../Components/PageHeader'
import { fetchData } from './../../../Actions/dataModelAction'
import {connect} from 'react-redux';

export class petList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      "current_page": 1,
      "queryParams": ''
    };
  }
  
  // get pet list from database
  componentWillMount() {
    this.props.fetchData('pet', this.props.match.params.pageNum, this.state.queryParams);
    this.setState({current_page: this.props.match.params.pageNum});
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.match.params.pageNum !== this.state.current_page){
      this.setState({current_page: nextProps.match.params.pageNum});
      this.props.fetchData('pet', nextProps.match.params.pageNum, this.props.queryParams); 
    }
  }
  
  render() {
    let pageNum = this.props.match.params.pageNum;
    return (
      <div className="album py-5 bg-light">
        <div className="container fillPage">
          <PageHeader heading="Pets" domain="/FindPets/"
           query={this.props.location.search}/>
          <br />
          <br />
          {/* HERE SET queryParams like ?animal=Dog&state=TX, then results will be filtered */}
          <Pets currentPage={pageNum} queryParams = {this.state.queryParams}/>
        </div>
        <PageBar domain="/FindPets/" pageNum={pageNum} totalPages={this.props.total_page}/>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  pets: state.pets.pet_items,
  total_page: state.pets.last_page,
  current_page: state.pets.current_page
});

export default connect(mapStateToProps, { fetchData })(petList)