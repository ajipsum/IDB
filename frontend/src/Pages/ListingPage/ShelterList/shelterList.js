import React, { Component } from 'react';
import Shelters from './../../../Components/Model/Shelters/Shelters';
import PageBar from './../../../Components/PageBar';
import axios from 'axios';
import PageHeader from './../../../Components/PageHeader';

export default class shelterList extends Component {
    constructor(props) {
        super(props);
        this.state = {
          "totalPages": 1,
        };
    }
      
      // get total number of pages
    componentWillMount() {
        const url = 'http://localhost:5000/api/shelter/page/total'
        axios.get(url, {
            headers: { 
                'Access-Control-Allow-Origin' : '*',
                'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',          
            },
            responseType: 'json',
        })
        .then(res => {
            this.setState({totalPages: res.data.total});
        });
    }

    render() {
        let pageNum = this.props.match.params.pageNum;
        return (
            <div className="album py-5 bg-light">
                <div className="container fillPage">                    
                    <PageHeader heading="Shelters" domain="/FindShelters/"
                     query={this.props.location.search}
                    />
                    <br />
                    <br />
                    <Shelters currentPage={pageNum}/>
                </div>
                <PageBar domain="/FindShelters/" pageNum={pageNum} totalPages = {this.state.totalPages}/>
            </div>
        )
    }
}
