import React, { Component } from 'react';
import Developer from './../Components/Developer';
import Tools from './../Components/Tools';
import './../CSS/About.css';

export default class About extends Component {
  constructor(props) {
    super(props);
    this.state= {
      'total_issue_count': 0,
      'total_commit_count': 0,
      'total_unit_tests': 0,
    }
  }

  incrementCommits(commit_count) {
    this.setState({total_commit_count: this.state.total_commit_count + commit_count});
  }

  incrementIssues(issue_count) {
    this.setState({total_issue_count: this.state.total_issue_count + issue_count});
  }

  incrementUnit(unit_count) {
    this.setState({total_unit_tests: this.state.total_unit_tests + unit_count});
  }

  render(){
    return (
      <div className="page-header">
        <div className="album py-5 bg-light">
          <div className="container">
            <div className="container">
              <div className="about-top-row">
                <div className="col-12 col-md-6">
                  <div className="jumbotron">
                    <h1>About the Site</h1>
                    <p>
                      ConnectPetsToMe is a site that allows prospective pet owners to find pets they may be interested in adopting,
                       nearby veterinary offices to vaccinate and care for their pet after adoption,
                       and animal shelters where visitors may volunteer. 
                    </p>
                  </div>
                </div>
                <div className="col-md-6 col 0">
                  <div className="jumbotron">
                      <h1>About our Data</h1>
                      <p>
                        Information on individual instances of pets, vets, and shelters was parsed programmatically from <a href="https://www.petfinder.com/">Petfinder</a>
                        , <a href="https://developers.google.com/maps/documentation/">Google</a>, 
                        and <a href="https://www.yelp.com/developers/documentation/v3">Yelp</a> via API calls. 
                        RESTful API documentation for our site is available <a href="https://documenter.getpostman.com/view/4670195/RWMCt9Mi"> here</a>,
                         and repository is available <a href="https://gitlab.com/CS373-18SU-GRP/IDB">here</a> .
                      </p>
                    </div>
                  </div>
              </div>
            </div>
            <h1 className="text-center">About the ConnectPetsToMe Team</h1>
            <div className="row" id="dev-cards">
              <Developer incrementCommits={ this.incrementCommits.bind(this) } incrementIssues = { this.incrementIssues.bind(this) } incrementUnit = { this.incrementUnit.bind(this) }/>
            </div>
            <div className="container">
              <div className="row">
                <div className="col-12 col-sm-3">
                  <ul className="list-group">
                    <li className="list-group-item">Total Commits: {this.state.total_commit_count} </li>
                    <li className="list-group-item">Total Issues: {this.state.total_issue_count} </li>
                    <li className="list-group-item">Total Unit Tests: {this.state.total_unit_tests} </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="album py-5 bg-light">
              <h1 className="text-center">Tools Used</h1>
              <div className="container">
                <div className="row">
                  <Tools />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
};
