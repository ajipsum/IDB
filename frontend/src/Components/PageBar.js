import React, { Component } from 'react';
import { Link } from 'react-router-dom';

let range = (start, stop) => {
  let i = start;
  let range = [];

  while (i < stop) {
    range.push(i++);
  }
  return range;
}

export default class PageBar extends Component{

  render() {
    const first = 1;
    const domain = this.props.domain
    const pageNumber = parseInt(this.props.pageNum, 10);
    const last = this.props.totalPages;
    const left = Math.max(first, pageNumber - 2);
    const right = Math.min(last + 1, pageNumber + 3);

    let leftButtons = range(left, pageNumber).map(num => {
      return (
        <li className="page-item" key={num}>
          <Link to={domain.concat(num)} className="page-link">{num}</Link> 
        </li>
      )
    });

    let rightButtons = range(pageNumber + 1, right).map(num => {
      return (
        <li className="page-item" key={num}> 
          <Link to={domain.concat(num)} className="page-link">{num}</Link> 
        </li>
      )
    });

    let lastPage = [
      (last - pageNumber) <= 3 ? null : 
        <li className="page-item" key="secondToLast">
          <div className="page-link disabled">...</div>
        </li>,
        <li className="page-item" key="last">
          <Link to={domain.concat(last)} className="page-link">{last}</Link>
        </li>
    ]

    return (
    <nav aria-label="Page navigation example">
      <ul className="pagination justify-content-center">
        <li className={pageNumber === first ? "page-item disabled" : "page-item"}>
          <Link to={domain.concat(first)} className="page-link">&laquo;</Link>
        </li>
        {leftButtons}
        <li className="page-item active">
          <div className="page-link disabled">{pageNumber} </div>
        </li>
        {rightButtons}
        {last - pageNumber > 2 ? lastPage : null}
        <li className={pageNumber === (last) ? "page-item disabled" : "page-item"}>
          <Link to={domain.concat(last)} className="page-link">&raquo;</Link>
        </li>
      </ul>
    </nav>
  )
}
}