import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './../CSS/Navbar.css';

class Navbar extends Component{
  render(){
    return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container">
          <Link className="navbar-brand" to="/">Connect Pets To Me</Link>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07" aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarsExample07">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link className="nav-link" to="/FindPets/1">Find Pets</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/FindVets/1">Find Vets</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/FindShelters/1">Find Shelters</Link>
              </li>
              <li className="nav-item">
                <Link id="about"  className="nav-link" to="/About">About Us</Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    )
  }
}

export default Navbar
