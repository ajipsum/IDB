import React, { Component } from 'react';
export default class InteractiveMap extends Component {
  constructor(props) {
    super(props);

    // default size = 250px, change if there is input
    this.mapSize = 250;
    if (this.props.size)
      this.mapSize = this.props.size;

    this.state = {
      url : 'https://maps.googleapis.com/maps/api/staticmap?center=',
      coord : String(this.props.latitude).concat(',', this.props.longitude),
      options : '&zoom=12&size='.concat(this.mapSize, 'x' ,this.mapSize, '&scale=2&markers=color:blue%7Csize:tiny%7Clabel:S%7C'),
      key : '&key=AIzaSyAVelkQ8ysqaPPt5UHxv_IOHuaYFBpbDiM',
      queryName : "https://www.google.com/maps/search/?api=1&query=".concat((this.props.name.split(" ").join("+"))),
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.name !== prevProps.name)
      this.setState({
      url : 'https://maps.googleapis.com/maps/api/staticmap?center=',
      coord : String(this.props.latitude).concat(',', this.props.longitude),
      options : '&zoom=12&size='.concat(this.mapSize, 'x' ,this.mapSize, '&scale=2&markers=color:blue%7Csize:tiny%7Clabel:S%7C'),
      key : '&key=AIzaSyAVelkQ8ysqaPPt5UHxv_IOHuaYFBpbDiM',
      queryName : "https://www.google.com/maps/search/?api=1&query=".concat((this.props.name.split(" ").join("+"))),
    });
  }

  render() {
    return (
      <div className="Map">
        <a href={this.state.queryName}>
          <img className="" src={this.state.url.concat(this.state.coord, this.state.options, this.state.coord, this.state.key)} alt=""/>
        </a>
      </div>
    )
  }
}