import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../../../CSS/ShelterItems.css'

export default class shelterItems extends Component {
  render() {

    const url = 'https://maps.googleapis.com/maps/api/staticmap?center=';
    const coord = String(this.props.shelterData.latitude).concat(',', this.props.shelterData.longitude);
    const options = '&zoom=12&size=150x150&scale=2&markers=color:blue%7Csize:tiny%7Clabel:S%7C';
    const key = '&key=AIzaSyAVelkQ8ysqaPPt5UHxv_IOHuaYFBpbDiM'
    return (
    <div className="col-md-4">
        <div className="card mb-4 box-shadow">
          <Link className="shelterimg" to={"/Shelters/ShelterEntity/".concat(this.props.shelterData.id)}><img className="card-img-top" src={url.concat(coord, options, coord, key)} alt=""/></Link>
          <div className="card-body">
            <p className="card-text"><Link to={"/Shelters/ShelterEntity/".concat(this.props.shelterData.id)}>{this.props.shelterData.name}</Link></p>
            {/*<small className="text-muted">11 miles away</small>*/}
            {/*<RatingStar rating={this.props.shelterData.rating}/>*/}
            <div className="d-flex justify-content-between align-items-center">
              <div className="btn-group">
                <button type="button" className="btn btn-sm btn-outline-secondary">{this.props.shelterData.city}</button>
                <button type="button" className="btn btn-sm btn-outline-secondary">{this.props.shelterData.phone}</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
