import React, {Component} from 'react';
import {connect} from 'react-redux';
import ShelterItems from './ShelterItems'
import { fetchData } from './../../../Actions/dataModelAction';

export class Shelters extends Component{

  // TO DO LIST
  // fetch data from backend based
  // this.props.petType will indicate either dog or cat etc
  // this.props.petType will be used for filtering

  // this is getting hard coded static data for phase 1
  // check dataModelAction.js
  componentWillMount(){
    this.props.fetchData('shelter', this.props.currentPage);
  }

  componentWillReceiveProps(nextProps){
    if (this.props.currentPage !== nextProps.currentPage){
      this.setState({currentPage: nextProps.currentPage});
      this.props.fetchData('shelter', nextProps.currentPage); 
    }
  }

  render(){
    let shelterItems = this.props.shelters.map(shelter => {
      return(
        <ShelterItems shelterData = {shelter} key={shelter.id}/>
      );
    });
    return (
      <div>
          <div className="row">
            {shelterItems}
          </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  shelters: state.shelters.shelter_items
});

export default connect(mapStateToProps, { fetchData })(Shelters)
