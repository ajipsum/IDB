import React, { Component } from 'react';

export default class VetItems extends Component {
  render() {
    return (
    <div className="col-md-4">
        <div className="card mb-4 box-shadow">
          <img className="card-img-top" src="https://s3.amazonaws.com/www.imdatabase.me/adult-care-cure-433635.jpg" alt="Card cap"/>
          <div className="card-body">
            <p className="card-text">{this.props.vetData.name}</p>
            <div className="d-flex justify-content-between align-items-center">
              <div className="btn-group">
                <button type="button" className="btn btn-sm btn-outline-secondary">{this.props.vetData.location}</button>
                <button type="button" className="btn btn-sm btn-outline-secondary">{this.props.vetData.rating}</button>
              </div>
              <small className="text-muted">9 mins</small>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
