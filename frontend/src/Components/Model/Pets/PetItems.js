import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../../../CSS/PetItems.css';


export default class PetItems extends Component {
  render() {
    let info = this.props.petData.id;
    return (
    <div className="col-md-4">
        <div className="card mb-4 box-shadow">
          <div className="petimg">
            <Link to={"/Pets/PetEntity/".concat(info)}><img className="card-img-top" src={this.props.petData.image_url} alt=""/></Link>
          </div>
          <div className="card-body">
          <p className="card-text"><Link  to={"/Pets/PetEntity/".concat(info)}>{this.props.petData.name}</Link></p>
          {/*<small className="text-muted">9 miles away</small>*/}
            <div className="d-flex justify-content-between align-items-center traits">
              <div className="btn-group">
                <button type="button" className="btn btn-sm btn-outline-secondary">{this.props.petData.age}</button>
                <button type="button" className="btn btn-sm btn-outline-secondary">{this.props.petData.breed}</button>
                <button type="button" className="btn btn-sm btn-outline-secondary">{this.props.petData.sex}</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
