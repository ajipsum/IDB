import React, {Component} from 'react';
import {connect} from 'react-redux';
import PetItems from './PetItems';

export class Pets extends Component{
  render(){
    let petItems = this.props.pets.map(pet => {
      return(
        <PetItems petData = {pet} key={pet.id}/>
      );
    });
   
    return (
      <div className="row">
        {petItems}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  pets: state.pets.pet_items,
  total_page: state.pets.last_page
});

export default connect(mapStateToProps)(Pets)
