import React, {Component} from 'react';
import {connect} from 'react-redux';
import VetItems from './VetItems';
import { fetchData } from './../../../Actions/dataModelAction';

export class Vets extends Component{

  // TO DO LIST
  // fetch data from backend based
  // this.props.petType will indicate either dog or cat etc
  // this.props.petType will be used for filtering

  // this is getting hard coded static data for phase 1
  // check dataModelAction.js
  componentWillMount(){
    this.props.fetchData('vet', this.props.currentPage);
  }

  componentWillReceiveProps(nextProps){
    if (this.props.currentPage !== nextProps.currentPage){
      this.setState({currentPage: nextProps.currentPage});
      this.props.fetchData('vet', nextProps.currentPage); 
    }
  }

  render(){
    let vetItems = this.props.vets.map(vet => {
      return(
        <VetItems vetData = {vet} key={vet.id}/>
      );
    });
    return (
      <div className="row">
        {vetItems}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  vets: state.vets.vet_items
});

export default connect(mapStateToProps, { fetchData })(Vets)

