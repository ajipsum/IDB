import React, { Component } from 'react';
import RatingStar from './../../../Components/RatingStar';
import { Link } from 'react-router-dom';
import '../../../CSS/VetItems.css'

export default class VetItems extends Component {
  render() {
    return (
    <div className="col-md-4">
        <div className="card mb-4 box-shadow">
          <div className="vetimg">
            <Link to={"/Vets/VetEntity/".concat(this.props.vetData.id)}><img className="card-img-top" src={this.props.vetData.image_url} alt=""/></Link>
          </div>
          <div className="card-body">
            <p className="card-text"><Link to={"/Vets/VetEntity/".concat(this.props.vetData.id)}>{this.props.vetData.name}</Link></p>
            {/*<small className="text-muted">9 miles away</small>*/}
            <RatingStar rating= {this.props.vetData.rating}/>
            <br/>
            <div className="d-flex justify-content-between align-items-center">
            
              <div className="btn-group">
                <button type="button" className="btn btn-sm btn-outline-secondary">{this.props.vetData.city}</button>
                <button type="button" className="btn btn-sm btn-outline-secondary">{this.props.vetData.display_phone}</button>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    )
  }
}
