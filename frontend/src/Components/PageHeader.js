import React, {Component} from 'react';
import {states, animals} from './../Assets/StaticData'
import {Link} from 'react-router-dom';
import axios from 'axios';
import queryString from 'query-string';

class PageHeader extends Component{
  constructor(props) {
    super(props);

    this.style = {
      backgroundColor: "#dcdcdc",
    }
  }

  render(){
    this.queryObject = queryString.parse(this.props.query);
    let resetCity = queryString.parse(this.props.query);
    delete(resetCity['city']);

    let resetState = queryString.parse(this.props.query);
    delete(resetState['city']);
    delete(resetState['state']);

    let resetAnimal = queryString.parse(this.props.query);
    delete(resetAnimal['animal']);

    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-light" style={this.style}>
          <span className="navbar-brand">
            {this.props.heading}
          </span>
          
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {this.queryObject.state === undefined ? "Filter By State" : "State: ".concat(this.queryObject.state)}
                </a>
                <div className="dropdown-menu scrollable-menu" aria-labelledby="navbarDropdown">
                  <Link className="dropdown-item" to={this.props.domain.concat('1?', queryString.stringify(resetState))}>Reset Filter</Link>
                  <DropItem domain={this.props.domain} type="states" queries={this.props.query}/>
                </div>
              </li>
              {this.queryObject.state !== undefined ? <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {this.queryObject.city === undefined ? "Filter By City" : "City: ".concat(this.queryObject.city)}
                </a>
                <div className="dropdown-menu scrollable-menu" aria-labelledby="navbarDropdown">
                  <Link className="dropdown-item" to={this.props.domain.concat('1?', queryString.stringify(resetCity))}>
                   Reset Filter
                  </Link>
                  <DropItem domain={this.props.domain} selectedState={this.queryObject.state} type="cities" queries={this.props.query}/>
                </div>
              </li> : null}
              {this.props.heading === 'Pets' ? 
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {this.queryObject.animal === undefined ? "Filter By Animal" : "Animal: ".concat(this.queryObject.animal)}
                </a>
                <div className="dropdown-menu scrollable-menu" aria-labelledby="navbarDropdown">
                  <Link className="dropdown-item" to={this.props.domain.concat('1?', queryString.stringify(resetAnimal))}>Reset Filter</Link>
                  <DropItem domain={this.props.domain} type="animals" queries={this.props.query}/>
                </div>
              </li> : null}
            </ul>{/*
            <form className="form-inline my-2 my-lg-0">
              <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
              <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>*/}
          </div>
        </nav>
      </div>
    )
  }
}

export class DropItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cities: [],
    }
  }

  componentWillMount() {
    if (this.props.type === "cities" && this.props.selectedState !== undefined) {
      axios.get('http://localhost:5000/api/pet/dropdown?state='.concat(this.props.selectedState), {
            headers: { 
                'Access-Control-Allow-Origin' : '*',
                'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',          
            },
            responseType: 'json',
      }).then(json => this.setState({cities: json['data'].city}));
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.type === 'cities' && this.props.selectedState !== prevProps.selectedState) {
      axios.get('http://localhost:5000/api/pet/dropdown?state='.concat(this.props.selectedState), {
            headers: { 
                'Access-Control-Allow-Origin' : '*',
                'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',          
            },
            responseType: 'json',
      }).then(json => this.setState({cities: json['data'].city}));
    }
  }

  render() {
    let items = [];
    let query = queryString.parse(this.props.queries);
    if (this.props.type === "states"){
      delete(query['city']);
      for (let state of states) {
        query['state'] = state;
        items.push(<Link className="dropdown-item state-item" to={this.props.domain.concat('1?', queryString.stringify(query))} key={state}>{state}</Link>);
      }
    }
    else if (this.props.type === "animals"){
      for (let animal of animals) {
        query['animal'] = animal;
        items.push(<Link className="dropdown-item state-item" to={this.props.domain.concat('1?', queryString.stringify(query))} key={animal}>{animal}</Link>);
      }
    }
    else if (this.props.type === "cities") {
      for (let city of this.state.cities) {
        query['city'] = city;
        items.push(<Link className="dropdown-item state-item" to={this.props.domain.concat('1?', queryString.stringify(query))} key={city}>{city}</Link>);
      }
    }
    return (
      <div>
        {items}
      </div>
    )
  }
}
export default PageHeader
