import ImageCard from './ImageCard';
import React, { Component } from 'react';

export default class VetPetLink extends Component {
  constructor(props){
    super(props);
    this.url = 'http://localhost:5000/api/shelter/'.concat(this.props.id);
    this.state = {
      pets: [],
    }
  }

  componentWillMount() {
    fetch(this.url).then(res => res.json())
    .then(json => this.setState({pets: json['shelter']['pets']}))
    .catch(error => console.error(error));
    }

  render() {
    const close_pets = [];
    if (this.state.pets.length !== 0)
      for (var i = 0; i < Math.min(4, this.state.pets.length); i++) {
        close_pets.push(
          <div className="col-md-3 d-flex">
            <ImageCard id={this.state.pets[i]} type={'pet'} key={this.state.pets[i]}/>
          </div>
          );
    }
    return (
      <div className="row">
        {close_pets}
      </div>
    )
  }
}