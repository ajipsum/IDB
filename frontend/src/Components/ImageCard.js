import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class ImageCard extends Component {
    constructor(props) {
        super(props);
        this.url = "";
        switch(this.props.type) {
            case 'vet': this.url = '/Vets/VetEntity/'; break;
            case 'pet': this.url = '/Pets/PetEntity/'; break;
            case 'shelter': this.url = '/Shelters/ShelterEntity/'; break;
            default: break;
        }
        this.state = {
            info: {}
        }
    }

    componentWillMount() {
        fetch("http://localhost:5000/api/".concat(this.props.type, "/", this.props.id), {
        }).then(res => res.json())
        .then(json => this.setState({info: json[this.props.type]}));
    }

    componentDidUpdate(prevProps) {
        if (prevProps.id !== this.props.id) {
            fetch("http://localhost:5000/api/".concat(this.props.type, "/", this.props.id), {
            }).then(res => res.json())
            .then(json => this.setState({info: json[this.props.type]}));
        }
    }
    render() {
        return (
            <div className="card mb-3 box-shadow">
              <div className="vetimg">
                <Link to={this.url.concat(this.state.info.id)}><img className="card-img-top" src={this.state.info.image_url !== undefined ? this.state.info.image_url : this.state.info.img_url} alt=""/></Link>
              </div>
                <p className="card-text"> {this.state.info.name} </p>
            </div>
        )
    }
}