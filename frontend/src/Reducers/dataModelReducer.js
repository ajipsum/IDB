import {FETCH_PET_PAGE, FETCH_VET_PAGE, FETCH_SHELTER_PAGE} from './../Actions/type';

const initialState = {
    pet_items: [],
    shelter_items: [],
    vet_items: [],
    item: {},
    current_page: 1,
    last_page: 1
};

export default function(state=initialState, action){
    switch(action.type){
        case FETCH_PET_PAGE:
            return{
                ...state,
                pet_items: action.data.objects,
                current_page: action.data.page,
                last_page: action.data.total_pages
            };
        case FETCH_VET_PAGE:
            return{
                ...state,
                vet_items: action.data.objects,
                current_page: action.data.page,
                last_page: action.data.total_pages
            };
        case FETCH_SHELTER_PAGE:
            return{
                ...state,
                shelter_items: action.data.objects,
                current_page: action.data.page,
                last_page: action.data.total_pages
            };
        default:
            return state;
    }
}
