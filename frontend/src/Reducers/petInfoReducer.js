import {FETCH_PET_ENTRY} from './../Actions/type';

const initialState = {
    items: [],
    item: {}
};

export default function(state=initialState, action){
    switch(action.type){
        case FETCH_PET_ENTRY:
            return{
                ...state,
                items: action.data.pet
            };
        default:
            return state;
    }
}
