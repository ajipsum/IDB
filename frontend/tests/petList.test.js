import expect from 'expect';
import petList from './../src/Pages/ListingPage/PetList/petList';
import React from 'react';
import { shallow } from 'enzyme';

describe("Tests petList.js", () => {

  test("Renders without exploding", () => {

    const wrapper = shallow(<petList />);

    expect(wrapper.length).toBe(1);
    expect(wrapper).toBeDefined();
  });

});
