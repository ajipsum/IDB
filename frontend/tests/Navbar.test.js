import expect from 'expect';
import Navbar from './../src/Components/Navbar';
import React from 'react';
import { shallow } from 'enzyme';

describe("Tests Navbar.js", () => {
  it("Renders without exploding", () => {
    const wrapper = shallow(<Navbar />);  
    expect(wrapper.length).toBe(1);
  });

  it("Contains five links", () => {
    const wrapper = shallow(<Navbar/>);
    expect(wrapper.find('Link').length).toBe(5);
  })
});
