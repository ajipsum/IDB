import expect from 'expect';
import vetList from './../src/Pages/ListingPage/VetList/vetList';
import React from 'react';
import { shallow, mount, render } from 'enzyme';

describe("Tests vetList.js", () => {

  test("Renders without exploding", () => {

    const wrapper = shallow(<vetList />);

    expect(wrapper.length).toBe(1);
    expect(wrapper).toBeDefined();
  });

});
