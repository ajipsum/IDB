#!/usr/bin/env python
"""
Test run for the gui of connectpetsto.me

"""
from sys import platform
import unittest
from unittest import TestCase
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import requests

# options = webdriver.ChromeOptions()
# options.add_argument('--headless')
if platform == "linux" or platform == "linux2":
    driver = webdriver.Chrome('./drivers/chromedriver_linux64')
elif platform == "darwin": # macOS
    driver=webdriver.Chrome('./drivers/chromedriver_mac64')
elif platform == "win32":
    driver=webdriver.Chrome('./drivers/chromedriver.exe')

driver.set_page_load_timeout(30)
driver.get("http://localhost:3000")
driver.maximize_window()
driver.implicitly_wait(5)

class MyUnitTests (TestCase):
    def test2(self):
        """
        Iterate through items in navbar to see if clickable
        """
        elements = driver.find_elements_by_class_name('nav-link')
        if not elements:
            self.fail("No nav bar elements found")
        for ele in elements:
            try:
                ele.click()
                pass
            except Exception as e:
                print(e)
                self.fail("This nav bar element is not clickable")

    def test3(self):
        """
        Test to see if the back to home button works from FindPets
        """
        driver.get("http://localhost:3000/FindPets/1")
        try:
            driver.find_element_by_class_name("navbar-brand").click()
            pass
        except NoSuchElementException:
            self.fail("The element does not exist")

    def test4(self):
        """
        Test to see if the back to home button works from FindVets
        """
        driver.get("http://localhost:3000/FindVets/1")
        try:
            driver.find_element_by_class_name("navbar-brand").click()
            pass
        except NoSuchElementException:
            self.fail("The element does not exist")

    def test5(self):
        """
        Test to see if the back to home button works from FindShelters
        """
        driver.get("http://localhost:3000/FindShelters/1")
        try:
            driver.find_element_by_class_name("navbar-brand").click()
            pass
        except NoSuchElementException:
            self.fail("The element does not exist")

    def test6(self):
        """
        Test to see if the back to home button works from About
        """
        driver.get("http://localhost:3000/About")
        try:
            driver.find_element_by_class_name("navbar-brand").click()
            pass
        except NoSuchElementException:
            self.fail("The element does not exist")

    def test7(self):
        """
        Test to see that every Pet card image on "Find Pets" page
        links to another page
        """
        driver.get("http://localhost:3000/FindPets/1")
        elements = driver.find_elements_by_class_name('petimg')

        if not elements:
            self.fail("No pet img elements found")
        for ele in elements:
            # driver.get("http://localhost:3000/FindPets")
            try:
                link = ele.find_element_by_tag_name("a").get_attribute("href")
                if link[0:5] == "https":
                    continue
                req = requests.get(link)
                if req.status_code >= 400:
                    self.fail("Bad link: " + str(link))
            except Exception as e:
                print(e, )
                self.fail("Something went wrong with testing to see if this " \
                        "pet img page was a broken link: " + str(link))

    def test8(self):
        """
        Test to see that every Vet card image on "Find Vets" page
        links to another page
        """
        driver.get("http://localhost:3000/FindVets/1")
        elements = driver.find_elements_by_class_name('vetimg')
        if not elements:
            self.fail("No vet img elements found")
        for ele in elements:
            # driver.get("http://localhost:3000/FindVets")
            try:
                link = ele.find_element_by_tag_name("a").get_attribute("href")
                if(link[0:5] == "https"):
                    continue
                req = requests.get(link)
                if req.status_code >= 400:
                    self.fail("Bad link: " + str(link))
            except Exception as e:
                print(e, )
                self.fail("Something went wrong with testing to see if this " \
                        "vet img page was a broken link: " + str(link))
    def test9(self):
        """
        Test to see that every Shelter card image on "Find Shelter" page
        links to another page
        """
        try:
            driver.get("http://localhost:3000/FindShelters/1")
        except Exception as e:
            self.fail("Could not open up FindShelters")
        elements = driver.find_elements_by_class_name("shelterimg")
        if not elements:
            self.fail("No shelter img elements found: " + str(elements))
        for ele in elements:
            # driver.get("http://localhost:3000/FindShelters/1")
            try:
                link = ele.find_element_by_tag_name("a").get_attribute("href")
                if link[0:5] == "https":
                    continue
                req = requests.get(link)
                if req.status_code >= 400:
                    self.fail("Bad link: " + str(link))
            except Exception as e:
                print(e, )
                self.fail("Something went wrong with testing to see if this " \
                        "pet shelter page was a broken link: " + str(link))

    def test10(self):
        """
        Check for broken links in home page
        """
        driver.get("http://localhost:3000")
        links = driver.find_elements_by_tag_name("a")
        if not links:
            self.fail("No anchor tags found")
        for link in links:
            try:
                # skip links with https link because we need a certificate
                if(link.get_attribute('href')[0:5] == "https"):
                    continue
                req = requests.get(link.get_attribute('href'))
                if req.status_code >= 400:
                    self.fail("Bad link: " + str(link.get_attribute('href')))
            except Exception as e:
                print(e)
                self.fail("Something went wrong with testing to see if home page " \
                        "had broken links: " + str(link.get_attribute('href')))

    def test11(self):
        """
        Check for broken links in FindPets page
        """
        driver.get("http://localhost:3000/FindPets/1")
        links = driver.find_elements_by_tag_name("a")
        if not links:
            self.fail("No anchor tags found")
        for link in links:
            try:
                # skip links with https link because we need a certificate
                if(link.get_attribute('href')[0:5] == "https"):
                    continue
                req = requests.get(link.get_attribute('href'))
                if req.status_code >= 400:
                    self.fail("Bad link: " + str(link.get_attribute('href')))
            except Exception as e:
                print(e)
                self.fail("Something went wrong with testing to see if FindPets " \
                        "page had broken links: " + str(link.get_attribute('href')))


    def test12(self):
        """
        Check for broken links in FindVets page
        """
        driver.get("http://localhost:3000/FindVets/1")
        links = driver.find_elements_by_tag_name("a")
        if not links:
            self.fail("No anchor tags found")
        for link in links:
            try:
                # skip links with https link because we need a certificate
                if(link.get_attribute('href')[0:5] == "https"):
                    continue
                req = requests.get(link.get_attribute('href'))
                if req.status_code >= 400:
                    self.fail("Bad link: " + str(link.get_attribute('href')))
            except Exception as e:
                print(e)
                self.fail("Something went wrong with testing to see if FindVets " \
                        "page had broken links: " + str(link.get_attribute('href')))

    def test13(self):
        """
        Check for broken links in FindShelters page
        """
        driver.get("http://localhost:3000/FindShelters/1")
        links = driver.find_elements_by_tag_name("a")
        if not links:
            self.fail("No anchor tags found")
        for link in links:
            try:
                # skip links with https link because we need a certificate
                if(link.get_attribute('href')[0:5] == "https"):
                    continue
                req = requests.get(link.get_attribute('href'))
                if req.status_code >= 400:
                    self.fail("Bad link: " + str(link.get_attribute('href')))
            except Exception as e:
                print(e)
                self.fail("Something went wrong with testing to see if FindShelters " \
                        "page had broken links: " + str(link.get_attribute('href')))

    def test14(self):
        """
        Check for broken links in About page
        """
        driver.get("http://localhost:3000/About")
        links = driver.find_elements_by_tag_name("a")
        if not links:
            self.fail("No anchor tags found")
        # links_checks = 0
        for link in links:
            # links_checks += 1
            try:
                # skip links with https link because we need a certificate
                if(link.get_attribute('href')[0:5] == "https"):
                    continue
                req = requests.get(link.get_attribute('href'))
                if req.status_code >= 400:
                    self.fail("Bad link: " + str(link.get_attribute('href')))
            except Exception as e:
                print(e, )
                self.fail("Something went wrong with testing to see if About page " \
                        "had broken links: " + str(link.get_attribute('href')))
    @classmethod
    def tearDownClass(cls):
        driver.close()
        driver.quit()


if __name__ == "__main__":
    unittest.main()
