import expect from 'expect';
import Home from './../src/Pages/Home/Home';
import React from 'react';
import { shallow } from 'enzyme';

const wrapper = shallow(<Home />);
describe("Tests Home.js", () => {
    test("Renders without exploding", () => {
      expect(wrapper.length).toBe(1);
    });
  });
