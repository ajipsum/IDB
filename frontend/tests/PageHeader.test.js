import expect from 'expect';
import PageHeader, {DropItem} from './../src/Components/PageHeader';
import React from 'react';
import { shallow } from 'enzyme';

describe("tests PageHeader.js", () => {
  it("Renders without exploding", () => {
    const wrapper = shallow(<PageHeader domain=""/>);  
    expect(wrapper.length).toBe(1);
  });

  it ("Renders the State menu but not the city menu.", () => {
    const wrapper = shallow(<PageHeader domain="" selectedState={null}/>);
    expect(wrapper.find('DropItem').length).toBe(1);
  });

  it ("Renders the State menu and the city menu if a state param is passed.", () => {
    const wrapper = shallow(<PageHeader domain="" query="?state=TX" />);
    expect(wrapper.find('DropItem').length).toBeGreaterThanOrEqual(2);
  });

  it ("Changes Filter text upon specifying a state.", () => {
    const wrapper = shallow(<PageHeader domain="" query="?state=TX" />);
    expect(wrapper.find('.dropdown-toggle').at(0).text()).toBe("State: TX");
  });

  it ("Changes Filter text upon specifying a state and city.", () => {
    const wrapper = shallow(<PageHeader domain="" query="?state=TX&city=Austin" />);
    expect(wrapper.find('.dropdown-toggle').at(1).text()).toBe("City: Austin");
  });
});

describe("tests DropItem component", () => {
  it("Renders without exploding", () => {
    const wrapper = shallow(<DropItem />);
    expect(wrapper.length).toBe(1);
  })

  it ("Renders all 50 states", () => {
    const wrapper = shallow(<DropItem type="states" domain=""/>);
    expect(wrapper.find('.state-item').length).toBeGreaterThanOrEqual(50);
  })
})