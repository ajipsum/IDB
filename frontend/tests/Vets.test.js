import expect from 'expect';
import {Vets} from './../src/Components/Model/Vets/Vets';
import React from 'react';
import { shallow, mount, render } from 'enzyme';

describe("Tests Vets.js", () => {

  let mockFetch = jest.fn();
  const minProps = {
    vets: [
        {
            id: "0",
            name: "Emancipet",
            image: "https://s3-media3.fl.yelpcdn.com/bphoto/wLB-AA3Dt15HeHs1OOxGGQ/ls.jpg",
            location: "Austin",
            rating: "4",
            contact: "(512) 587-7729"
        },
        {
            id: "1",
            name: "Zippivet",
            image: "https://s3-media1.fl.yelpcdn.com/bphoto/S2LYJ96acZeO62hfcfzHjQ/ls.jpg",
            location: "Austin",
            rating: "4.5",
            contact: "(512) 904-0218"
        },
        {
            id: "2",
            name: "PAZ Veterinary",
            image: "https://s3-media4.fl.yelpcdn.com/bphoto/MvDi_2SFJk8WYtCdRCnIEA/o.jpg",
            location: "Austin",
            rating: "5",
            contact: "(512) 236-8000"
        },
        {
            id: "3",
            name: "Austin Urban Vet Center ",
            image: "https://s3-media1.fl.yelpcdn.com/bphoto/6cnIoBmQyIFAtwCghNODDg/ls.jpg",
            location: "Austin",
            rating: "4",
            contact: "(512) 476-2882"
        }
    ],
    fetchData: mockFetch
  }

  test("Renders without exploding", () => {
    let mockFetch = jest.fn();
    const wrapper = shallow(<Vets {...minProps} fetchData={mockFetch}/>);

    expect(wrapper.length).toBe(1);
    expect(wrapper).toBeDefined();
    expect(mockFetch).toHaveBeenCalled();
    // expect(mockFetch.mock.calls[0]).toEqual(['data'])
  });

});
