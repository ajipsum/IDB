import expect from 'expect';
import {ShelterEntity} from './../src/Pages/Entities/ShelterEntity';
import React from 'react';
import { shallow } from 'enzyme';
import { fetchShelterEntity } from './../src/Actions/dataModelAction';

describe("Tests ShelterEntity.js", () => {

  let mockFetch = jest.fn();
  const minProps = {
    shelterInfo: {
      address1: null, 
      address2: null, 
      city: "Lombard", 
      close_vets: [],
      email: "animalheartline@hotmail.com",
      id: "IL115",
      latitude: 41.8785,
      longitude: -88.0124,
      name: "Animal Heartline Humane Association",
      pets: ["40734569", "41352679", "41428034", "42098125", "42136732"],
      phone: "630-341-3411",
      state: "IL",
      zip: "60148",
    },
    fetchShelterEntity: {mockFetch},
    match:{params: {shelterId:"123"}}
  }

  test("should call fetchPetEntity when mounted", () => {
    
    let mockFetch = jest.fn();
    const wrapper = shallow(<ShelterEntity {...minProps} fetchShelterEntity={mockFetch}/>);
    
    expect(wrapper.length).toBe(1);
    expect(wrapper).toBeDefined();
    expect(mockFetch).toHaveBeenCalled();
  });

});
