import expect from 'expect';
import App from './../src/App';
import React from 'react';
import { shallow } from 'enzyme';

const wrapper = shallow(<App />);

describe("Tests App.js", () => {
  test("Renders without exploding", () => {
    expect(wrapper.length).toBe(1);
  });

  test("Has at least 8 routes", () => {
    const wrapper = shallow(<App />);
    expect(wrapper.find('Route').length).toBeGreaterThanOrEqual(8);
  })
});
