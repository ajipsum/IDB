import expect from 'expect';
import shelterList from './../src/Pages/ListingPage/ShelterList/shelterList';
import React from 'react';
import { shallow, mount, render } from 'enzyme';

describe("Tests shelterList.js", () => {

  test("Renders without exploding", () => {

    const wrapper = shallow(<shelterList />);

    expect(wrapper.length).toBe(1);
    expect(wrapper).toBeDefined();
  });

});
